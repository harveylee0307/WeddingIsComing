document.addEventListener('click', event => {
    bursty(event.pageX, event.pageY)
})

setInterval(() => randomizedConfetti(), 1000)

function bursty(x, y) {
    const burst = new mojs.Burst({
        left: 0,
        top: 0,
        radius: { 0: 200 },
        count: 25,
        degree: 360,
        children: {
            fill: { white: '#CE3349' },
            duration: 2000,
        },
        onComplete: function () {
            this.el.parentNode.removeChild(this.el)
        },
    }).tune({
        x: x,
        y: y,
    })

    burst.replay()
}

function randomizedConfetti() {
    let randomX = Math.floor(Math.random() * (document.body.clientWidth - 100) + 0)
    let randomY = Math.floor(Math.random() * (window.innerHeight - 200) + 0)
    const burst = new mojs.Burst({
        left: 0,
        top: 0,
        radius: { 0: 200 },
        count: 25,
        degree: 360,
        children: {
            fill: { white: '#FFDB66' },
            duration: 2000,
        },
        onComplete: function () {
            this.el.parentNode.removeChild(this.el)
        },
    }).tune({
        x: randomX,
        y: randomY,
    })

    burst.replay()
}

Splitting()

$('#time')
    .countdown('2022/07/03 12:00')
    .on('update.countdown', function (event) {
        let $this = $(this).html(
            event.strftime(
                '' +
                    '<span class="h1">%D</span> Day%!d' +
                    '<span class="h1">%H</span> Hr' +
                    '<span class="h1">%M</span> Min' +
                    '<span class="h1">%S</span> Sec',
            ),
        )
    })
$('.map-responsive').slideToggle(500, 'swing')
$('.comments-responsive').slideToggle(500, 'swing')

$('.card').on('click ', function () {
    $(this).toggleClass('open')
})
$('.comments-toggle,.fixed-btn').on('click', function () {
    $('.comments-responsive').slideToggle(500, 'swing')
    $('body').toggleClass('comments-open')
})
$('.map-toggle').on('click', function () {
    $('.map-responsive').slideToggle(500, 'swing')
})

function GetRequest() {
    let url = location.search
    let theRequest = new Object()
    if (url.indexOf('?') != -1) {
        let str = url.substr(1)
        strs = str.split('&')
        for (let i = 0; i < strs.length; i++) {
            theRequest[strs[i].split('=')[0]] = decodeURI(strs[i].split('=')[1])
        }
    }
    return theRequest
}

let url = location.href

if (url.indexOf('?') != -1) {
    var query = GetRequest()
    $('.opening__info .name').text(query.name)
    switch (query.type) {
        case '1':
            $('.opening__info .type').text('先生')
            break
        case '2':
            $('.opening__info .type').text('小姐')
            break
        case '3':
            $('.opening__info .type').text('全家福')
            break
        default:
            break
    }
} else {
    $('.opening').remove()
}

$('.btn-close-open').on('click', function () {
    $('.opening').fadeOut(500, 'swing')
})
