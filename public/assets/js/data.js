
<script type="module">
    // Import the functions you need from the SDKs you need
    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
    import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-analytics.js";
    import { getFirestore, collection, doc, getDoc } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js'
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
        apiKey: "AIzaSyAhgPKknKrHvYbF6ZUxqAie86GjaATY__g",
        authDomain: "wedding-is-coming.firebaseapp.com",
        projectId: "wedding-is-coming",
        storageBucket: "wedding-is-coming.appspot.com",
        messagingSenderId: "767129703391",
        appId: "1:767129703391:web:03869572b79d4421d0aa6a",
        measurementId: "G-35PNZ7TYLX"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);
    const db = getFirestore(app);



    const docRef = doc(collection(db, 'clickCounts'));
    const docCounts = await getDoc(docRef);

    if (docCounts.exists()) {
        console.log("Document data:", docCounts.data());
    } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
    }

</script>